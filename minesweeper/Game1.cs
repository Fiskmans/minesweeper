﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.IO;
using System.Collections.Generic;

namespace minesweeper
{
    public class Game1 : Game
    {
        enum State {Unseen,seen,dead,flagged,blownup,mine}

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Texture2D[] Mines;
        Texture2D Full;
        Texture2D Flag;
        Texture2D Bomb;
        Texture2D Blown;

        int[,] Playfield;
        State[,] StateField;
        bool[,] BombField;

        int width = 80;
        int height = 80;

        int drawwidth = 20;
        int drawheight = 20;

        int windowwidth = 800;
        int windowHeight = 600;

        float Bombdensity = 0.15f;
        int bombs;

        string FolderPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Minesweeper\";
        string error;
        bool FieldChanged = false;

        Random rng;

        Vector2 Offset = Vector2.Zero;

        Point[] Around;
        Rectangle bounds;

        MouseState oldms;
        KeyboardState oldks;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }
        
        protected override void Initialize()
        {
            bombs = (int)(Bombdensity * width * height);
            graphics.PreferredBackBufferWidth = windowwidth;
            graphics.PreferredBackBufferHeight = windowHeight;
            graphics.ApplyChanges();
            
            bounds = new Rectangle(0, 0, width, height);
            Around = new Point[8];
            int index = 0;
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    if (i != 0 || j != 0)
                    {
                        Around[index] = new Point(i, j);
                        index++;
                    }
                }
            }
            oldms = Mouse.GetState();
            oldks = Keyboard.GetState();
            IsMouseVisible = true;
            // TODO: Add your initialization logic here
            Playfield = new int[width,height];
            StateField = new State[width, height];
            BombField = new bool[width, height];
            rng = new Random();
            for (int i = 0; i < bombs; i++)
            {

                int x = rng.Next(width);
                int y = rng.Next(height);
                while (BombField[x,y])
                {
                    x = rng.Next(width);
                    y = rng.Next(height);
                }
                BombField[x, y] = true;
                StateField[x, y] = State.mine;
            }


            for (int i = 0; i < Playfield.GetLength(0); i++)
            {
                for (int j = 0; j < Playfield.GetLength(1); j++)
                {
                    Playfield[i,j] = FindState(i,j,State.mine);
                }
            }
            for (int i = 0; i < Playfield.GetLength(0); i++)
            {
                for (int j = 0; j < Playfield.GetLength(1); j++)
                {
                    StateField[i, j] = State.Unseen;
                }
            }

            Directory.CreateDirectory(FolderPath);
            File.WriteAllLines(FolderPath + "Move.template.txt", new string[] { "[x (int)]", "[y (int)]", "[leftclick/rightclick]", "", "Fill out a text file with this information called 'move.txt' to make a move","File will be deleted after move has been made"});
            File.WriteAllLines(FolderPath + "PlayField.template.txt", new string[] { "[Width as number]", "[Height as number]", "[Playfield as single string composed of numbers base 10 seperated by commas (can be negative)]","","-4: error", "-3: tile has flag", "-2: tile has bomb", "-1: tile is not exposed", "0-8: amount of bombs around tile" });

            base.Initialize();
        }
        
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            Mines = new Texture2D[10];
            Full = Content.Load<Texture2D>("Full");
            Flag = Content.Load<Texture2D>("Flag");
            Bomb = Content.Load<Texture2D>("Bomb");
            Blown = Content.Load<Texture2D>("Blown");
            for (int i = 0; i < Mines.Length; i++)
            {
                Mines[i] = Content.Load<Texture2D>("Mine " + i);
            }

            // TODO: use this.Content to load your game content here
        }
        
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        protected override void Update(GameTime gameTime)
        {
            MouseState ms = Mouse.GetState();
            KeyboardState ks = Keyboard.GetState();
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            drawwidth = ms.ScrollWheelValue/100 + 20;
            drawheight = ms.ScrollWheelValue/100 + 20;

            if (ks.IsKeyUp(Keys.Space) && ms.LeftButton == ButtonState.Pressed && oldms.LeftButton == ButtonState.Released)
            {
                Click((ms.Y-(int)Offset.Y) / drawheight, (ms.X - (int)Offset.X) / drawwidth, Button.Left);
            }
            if (ks.IsKeyDown(Keys.Space) && ms.LeftButton == ButtonState.Pressed)
            {
                Offset = Offset + ms.Position.ToVector2() - oldms.Position.ToVector2();
            }

            if (ms.RightButton == ButtonState.Pressed && oldms.RightButton == ButtonState.Released)
            {
                Click((ms.Y - (int)Offset.Y) / drawheight, (ms.X - (int)Offset.X) / drawwidth, Button.Right);
            }
            if (ks.IsKeyDown(Keys.R) && oldks.IsKeyUp(Keys.R))
            {
                Initialize();
            }
            ImportMove();
            if (FieldChanged)
            {
                FieldChanged = false;
                Export();
            }

            oldms = ms;
            oldks = ks;
            base.Update(gameTime);
        }

        enum Button {Left,Right}
        void Click(int x, int y, Button b)
        {
            FieldChanged = true;
            if (bounds.Contains(new Point(x,y)))
            {
                switch (b)
                {
                    case Button.Left:
                        if (StateField[x, y] == State.Unseen)
                        {
                            if (BombField[x,y])
                            {
                                StateField[x, y] = State.blownup;
                            }
                            else
                            {
                                StateField[x, y] = State.seen;
                            }
                            
                        }
                        if (StateField[x,y] == State.seen)
                        {
                            if (FindState(x, y, State.flagged) + FindState(x,y,State.blownup) >= Playfield[x, y])
                            {
                                StateField[x, y] = State.dead;
                                foreach (Point p in Around)
                                {
                                    Click(x + p.X, y + p.Y, Button.Left);
                                }
                            }
                        }
                        break;
                    case Button.Right:
                        if (StateField[x, y] == State.Unseen)
                        {
                            StateField[x, y] = State.flagged;
                        }
                        else if (StateField[x, y] == State.flagged)
                        {
                            StateField[x, y] = State.Unseen;
                            foreach (Point p in Around)
                            {
                                if (bounds.Contains(new Point(x+p.X,y+p.Y)) && StateField[x + p.X,y + p.Y] == State.dead)
                                {
                                    StateField[x + p.X, y + p.Y] = State.seen;
                                }
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        int FindState(int x, int y,State s)
        {
            int output = 0;
            foreach (Point p in Around)
            {
                if (bounds.Contains(new Point(x + p.X,y + p.Y)) && StateField[x+p.X,y+p.Y] == s) 
                {
                    output++;
                }
            }
            return output;
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            int Flags = 0;

            spriteBatch.Begin();
            for (int i = 0; i < Playfield.GetLength(0); i++)
            {
                for (int j = 0; j < Playfield.GetLength(1); j++)
                {
                    Rectangle drawrect = new Rectangle(j * drawwidth + (int)Offset.X, i * drawheight + (int)Offset.Y, drawwidth, drawheight);
                    switch (StateField[i,j])
                    {
                        case State.Unseen:
                            spriteBatch.Draw(Full, drawrect, Color.White);
                            break;
                        case State.seen:
                            spriteBatch.Draw(Mines[Playfield[i, j]], drawrect, Color.White);
                            break;
                        case State.flagged:
                            Flags++;
                            spriteBatch.Draw(Flag, drawrect, Color.White);
                            break;
                        case State.blownup:
                            Flags++;
                            spriteBatch.Draw(Blown, drawrect, Color.White);
                            break;
                        case State.mine:
                            spriteBatch.Draw(Bomb, drawrect, Color.White);
                            break;
                        case State.dead:
                            spriteBatch.Draw(Mines[0], drawrect, Color.White);
                            spriteBatch.Draw(Mines[Playfield[i, j]], drawrect, Color.White*0.2F);
                            break;
                        default:
                            break;
                    }
                }
            }

            DrawBombsLeft(spriteBatch, windowwidth - drawwidth / 3 * 4, drawheight / 3, bombs - Flags);

            spriteBatch.End();

            base.Draw(gameTime);
        }

        void DrawBombsLeft(SpriteBatch s,int x,int y,int number)
        {
            int depth = 0;
            s.Draw(Bomb, new Rectangle(x, y, drawwidth, drawheight), Color.White);
            while (number > 0)
            {
                s.Draw(Mines[number % 10], new Rectangle(x - drawwidth * depth - drawwidth, y, drawwidth, drawheight), Color.White);
                number /= 10;
                depth++;
            }
        }

        void Export()
        {
            string[] output = new string[4];
            output[0] = width.ToString();
            output[1] = height.ToString();

            for (int i = 0; i < Playfield.GetLength(0); i++)
            {
                for (int j = 0; j < Playfield.GetLength(1); j++)
                {
                    switch (StateField[i,j])
                    {
                        case State.Unseen:
                            output[2] += ",-1";
                            break;
                        case State.seen:
                        case State.dead:
                            output[2] += "," + Playfield[i, j].ToString();
                            break;
                        case State.flagged:
                            output[2] += ",-3";
                            break;
                        case State.blownup:
                        case State.mine:
                            output[2] += ",-2";
                            break;
                        default:
                            output[2] += ",-4";
                            break;
                    }
                }
            }
            output[2] = output[2].Substring(1);
            output[3] = bombs.ToString();
            try
            {
                Directory.CreateDirectory(FolderPath);
                File.WriteAllLines(FolderPath + "PlayField.txt", output);
            }
            catch (Exception e)
            {
                error = e.Message;
            }
        }

        void ImportMove()
        {
            try
            {
                string[] files = Directory.GetFiles(FolderPath);
                bool exists = false;
                foreach (string s in files)
                {
                    if (s == FolderPath + "Move.txt")
                    {
                        exists = true;
                    }
                }
                if (exists)
                {
                    string[] move = File.ReadAllLines(FolderPath + "Move.txt");
                    if (move.Length > 2)
                    {
                        int x = int.Parse(move[0]);
                        int y = int.Parse(move[1]);
                        Button b = Button.Left;
                        if (char.ToLower(move[2][0],System.Globalization.CultureInfo.CurrentCulture) == 'r')
                        {
                            b = Button.Right;
                        }
                        Click(x, y, b);
                        File.Delete(FolderPath + "Move.txt");
                    }
                }
            }
            catch (Exception e)
            {
                //something went wrong, too bad
                error = e.Message;
                Console.WriteLine(e.Message);
            }
        }
    }
}
